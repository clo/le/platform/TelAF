# Usage

Just follow steps below:

```shell
# First, setup the basic environment for the dcgen tool.
# Only needed by the first time
$ make setup

# For the full feature of diag configuration
$ make full-feature
# or just:
$ make

# For the installation about the full feature
$ make install-full-feature
# or just:
$ make install
```
