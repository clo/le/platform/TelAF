# Copyright (c) 2023 Qualcomm Innovation Center, Inc. All rights reserved.
# SPDX-License-Identifier: BSD-3-Clause-Clear

ARG UBUNTU_DISTRO_ORIGIN
FROM $UBUNTU_DISTRO_ORIGIN

RUN apt-get -qq update
RUN apt-get -y install sudo
RUN apt-get update --fix-missing

RUN apt-get -y --no-install-recommends install ca-certificates
RUN update-ca-certificates
RUN apt-get -y --no-install-recommends install build-essential
RUN apt-get -y --no-install-recommends install curl
RUN apt-get -y --no-install-recommends install python3
RUN apt-get -y --no-install-recommends install ninja-build
RUN apt-get -y --no-install-recommends install python-jinja2
RUN DEBIAN_FRONTEND=noninteractive apt-get -y --no-install-recommends install cmake
RUN apt-get -y --no-install-recommends install python-setuptools
RUN apt-get -y --no-install-recommends install python
RUN apt-get -y --no-install-recommends install git
RUN apt-get -y --no-install-recommends install fakeroot
RUN apt-get -y --no-install-recommends install file
RUN apt-get -y --no-install-recommends install libcap-dev
RUN apt-get -y --no-install-recommends install libssl-dev
RUN DEBIAN_FRONTEND=noninteractive apt-get -y --no-install-recommends install wget
RUN DEBIAN_FRONTEND=noninteractive apt-get -y --no-install-recommends install git
RUN apt-get -y --no-install-recommends install python-dev
RUN DEBIAN_FRONTEND=noninteractive apt-get -y --no-install-recommends install unzip
RUN DEBIAN_FRONTEND=noninteractive apt-get -y --no-install-recommends install openjdk-8-jre-headless

RUN apt-get -y --no-install-recommends install python3-venv
RUN DEBIAN_FRONTEND=noninteractive apt-get install -y --no-install-recommends \
        software-properties-common \
    && add-apt-repository -y ppa:deadsnakes \
    && DEBIAN_FRONTEND=noninteractive apt-get install -y --no-install-recommends \
        python3.8-venv python3.8 python3.8-dev python3.8-dbg
RUN /usr/bin/python3.8 -m venv /venv
ENV PATH=/venv/bin:$PATH
RUN python3 -m pip install doipclient==1.1.1
RUN python3 -m pip install udsoncan==1.23.2
RUN python3 -m pip install pyyaml==6.0.1
RUN python3 -m pip install MarkupSafe==2.0.1
RUN python3 -m pip install Jinja2==3.0.3
RUN python3 -m pip install oyaml==1.0

RUN apt-get -y --no-install-recommends install pkg-config
RUN apt-get -y --no-install-recommends install libsqlite3-dev
RUN apt-get -y --no-install-recommends install libasound2-dev
RUN apt-get -y --no-install-recommends install nano
RUN apt-get -y --no-install-recommends install libglib2.0-dev

ARG DEVELOPER_UID=1000
ARG DEVELOPER_GID=1000

# Create a non-root user: [developer] for development, with password: [simula]
RUN addgroup --gid $DEVELOPER_GID developer && \
    adduser --uid $DEVELOPER_UID --ingroup developer --home /home/developer \
            --shell /bin/bash --disabled-password --gecos "" developer

RUN adduser developer sudo
RUN echo 'developer:simula' | chpasswd
RUN echo 'developer ALL=(ALL:ALL) ALL' >> /etc/sudoers

WORKDIR /home/developer/
USER developer:developer

RUN echo 'if [ "`id -u`" -eq 0 ]; then PS1="[Build Your Simulation] \\u:\\w # "; else PS1="[Build Your Simulation] \\u:\\w $ "; fi' >> ~/.bashrc
ENTRYPOINT ["/bin/bash", "/home/developer/simulation_ro/scripts/forward_to_container.sh"]
