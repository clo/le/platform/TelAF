/*
 * Copyright (c) 2024 Qualcomm Innovation Center, Inc. All rights reserved.
 * SPDX-License-Identifier: BSD-3-Clause-Clear
 */

#ifndef __CONFIGURATION_HPP__
#define __CONFIGURATION_HPP__

/* Auto-generated file.  DO NOT EDIT !! */
/* Generated by tafDiagGen tool */

#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>

#include <string>
#include <vector>
#include <map>
#include <iostream>
#include <cstdint>
#include <memory>


#define EXPORT_SYM __attribute__((visibility ("default")))

namespace telux {
namespace tafsvc {
namespace cfg {

typedef boost::property_tree::ptree Node;

/* Definitions for Enable-Condition-ID & Event-ID ... */
#include "diag_ids.h"

/* Operation Cycle IDs should be equal 'diag_ids.h' */
typedef enum operation_cycle_type
{
    IGNITION,
    DC,
    POWER
} operation_cycle_type_t;

inline static operation_cycle_type_t s_to_operation_cycle_type(std::string s) {

    if (std::string("IGNITION") == s)
    {
        return IGNITION;
    }
    else if (std::string("DC") == s)
    {
        return DC;
    }
    else if (std::string("POWER") == s)
    {
        return POWER;
    }

    throw std::runtime_error("Not match any operation_cycle_type_t");
}

typedef enum freeze_frame_trigger_type
{
    DEM_TRIGGER_ON_CONFIRMED,
    DEM_TRIGGER_ON_EVERY_TEST_FAILED,
    DEM_TRIGGER_ON_FDC_THRESHOLD,
    DEM_TRIGGER_ON_PENDING,
    DEM_TRIGGER_ON_TEST_FAILED,
    DEM_TRIGGER_ON_TEST_FAILED_THIS_OPERATION_CYCLE
} freeze_frame_trigger_type_t;

inline static freeze_frame_trigger_type_t s_to_freeze_frame_trigger_type(std::string s) {

    if (std::string("DEM_TRIGGER_ON_CONFIRMED") == s)
    {
        return DEM_TRIGGER_ON_CONFIRMED;
    }
    else if (std::string("DEM_TRIGGER_ON_EVERY_TEST_FAILED") == s)
    {
        return DEM_TRIGGER_ON_EVERY_TEST_FAILED;
    }
    else if (std::string("DEM_TRIGGER_ON_FDC_THRESHOLD") == s)
    {
        return DEM_TRIGGER_ON_FDC_THRESHOLD;
    }
    else if (std::string("DEM_TRIGGER_ON_PENDING") == s)
    {
        return DEM_TRIGGER_ON_PENDING;
    }
    else if (std::string("DEM_TRIGGER_ON_TEST_FAILED") == s)
    {
        return DEM_TRIGGER_ON_TEST_FAILED;
    }
    else if (std::string("DEM_TRIGGER_ON_TEST_FAILED_THIS_OPERATION_CYCLE") == s)
    {
        return DEM_TRIGGER_ON_TEST_FAILED_THIS_OPERATION_CYCLE;
    }

    throw std::runtime_error("Not match any freeze_frame_trigger_type_t");
}

typedef enum freeze_frames_type
{
    firstOccurrence,
    lastOccurrence
} freeze_frames_type_t;

typedef enum extended_data_records_type
{
    OccurrenceCounter,
    CumulativeDistanceWithTestFailed
} extended_data_records_type_t;

inline static extended_data_records_type_t s_to_extended_data_records_type(std::string s) {

    if (std::string("OccurrenceCounter") == s)
    {
        return OccurrenceCounter;
    }
    else if (std::string("CumulativeDistanceWithTestFailed") == s)
    {
        return CumulativeDistanceWithTestFailed;
    }

    throw std::runtime_error("Not match any extended_data_records_type_t");
}

typedef enum debounce_algorithm_type
{
    Counter,
    Timer,
    Custom
} debounce_algorithm_type_t;


typedef struct { /* <-- from [debounce_counter_based_algorithm] */
    std::string short_name;
    bool counter_based;
    bool debounce_counter_storage;
    std::string debounce_behavior;
    int counter_decrement_step_size;
    int counter_passed_threshold;
    int counter_increment_step_size;
    int counter_failed_threshold;
    int counter_jump_down_value;
    int counter_jump_up_value;
    bool counter_jump_up;
    bool counter_jump_down;
    std::string base;
    int counter_fdc_threshold;
} Counter_t;

typedef struct { /* <-- from [debounce_time_based_algorithm] */
    std::string short_name;
    bool time_based;
    float time_failed_threshold;
    float time_passed_threshold;
    std::string base;
    float time_fdc_threshold;
    std::string debounce_behavior;
} Timer_t;

typedef struct { /* <-- from [debounceCustom] */
    std::string short_name;
    bool monitor_internal;
    std::string base;
} Custom_t;


typedef struct {
    int max_number_of_request_correctly_received_response_pending;
    std::string occurrence_counter_processing;
    float s3_server_max;
    bool ignore_request_for_hardreset;
    int dtc_status_availability_mask;
} common_props_t;


EXPORT_SYM void diag_config_init(const char * cfg_path);

EXPORT_SYM bool get_init_status(void);

#define NEED_INITED() \
do { \
    if (get_init_status() != true) \
    { \
        throw std::runtime_error("Configuration is not inited."); \
    } \
} while (0)

template <typename T>
Node & match_item
(
    Node & item_container,
    const std::string & field_name,
    T & expected_value
)
{
    for (auto & item: item_container)
    {
        T field_value = item.second.get<T>(field_name);

        if (field_value == expected_value)
        {
            return item.second;
        }
    }

    throw std::runtime_error("Not found any item to match.");
}

EXPORT_SYM Node & get_root_node(void);
EXPORT_SYM Node & get_dtc_node(uint32_t dtc_code);
EXPORT_SYM Node & get_dtc_node(uint16_t event_id);
EXPORT_SYM Node & get_event_node(uint16_t event_id);

EXPORT_SYM std::map<uint16_t, std::shared_ptr<Node>> get_event_nodes(uint32_t dtc_code);
EXPORT_SYM std::vector<uint16_t> get_event_ids(uint32_t dtc_code);
EXPORT_SYM std::map<uint32_t, std::shared_ptr<Node>> get_dtc_nodes(void);
EXPORT_SYM std::vector<uint32_t> get_dtc_codes(void);

EXPORT_SYM size_t get_did_value_size(uint16_t did_code);

EXPORT_SYM std::map<std::string, uint8_t> get_diagnostic_session_map(void);

EXPORT_SYM uint8_t get_nrc_by_condition_id(uint8_t cond_id);

EXPORT_SYM uint8_t get_security_level_id(std::string level_name);

EXPORT_SYM std::shared_ptr<std::vector<uint8_t>>
get_pattern_sessions_by_session_id(uint8_t session_id);

EXPORT_SYM std::shared_ptr<std::vector<uint8_t>>
get_pattern_levels_by_session_id(uint8_t session_id);

EXPORT_SYM bool is_forbidden(uint16_t did_code, const uint8_t *payload, uint32_t plen);

template <typename T>
static inline void fill_list(Node & node, std::vector<T> & to_be_filled)
{
    for (auto &n : node) {
        to_be_filled.push_back(n.second.get_value<T>());
    }
}

template <typename T = std::string>
Node & top_debounce_algorithm(std::string field_name, T expected_value)
{
    Node & das = get_root_node().get_child("debounce_algorithm");
    return match_item<T>(das, field_name, expected_value);
}

template <typename T = std::string>
void top_debounce_algorithm(std::string field_name, T expected_value, Counter_t* to_be_filled)
{
    if (to_be_filled == nullptr)
    {
        throw std::runtime_error("[to_be_filled] is nullptr in [top_debounce_algorithm]");
    }

    Node & node = top_debounce_algorithm<T>(field_name, expected_value);

    to_be_filled->short_name = node.get<std::string>("short_name");
    to_be_filled->counter_based = node.get<bool>("counter_based");
    to_be_filled->debounce_counter_storage = node.get<bool>("debounce_counter_storage");
    to_be_filled->debounce_behavior = node.get<std::string>("debounce_behavior");
    to_be_filled->counter_decrement_step_size = node.get<int>("counter_decrement_step_size");
    to_be_filled->counter_passed_threshold = node.get<int>("counter_passed_threshold");
    to_be_filled->counter_increment_step_size = node.get<int>("counter_increment_step_size");
    to_be_filled->counter_failed_threshold = node.get<int>("counter_failed_threshold");
    to_be_filled->counter_jump_down_value = node.get<int>("counter_jump_down_value");
    to_be_filled->counter_jump_up_value = node.get<int>("counter_jump_up_value");
    to_be_filled->counter_jump_up = node.get<bool>("counter_jump_up");
    to_be_filled->counter_jump_down = node.get<bool>("counter_jump_down");
    to_be_filled->base = node.get<std::string>("base");
    to_be_filled->counter_fdc_threshold = node.get<int>("counter_fdc_threshold");

}

template <typename T = std::string>
void top_debounce_algorithm(std::string field_name, T expected_value, Timer_t* to_be_filled)
{
    if (to_be_filled == nullptr)
    {
        throw std::runtime_error("[to_be_filled] is nullptr in [top_debounce_algorithm]");
    }

    Node & node = top_debounce_algorithm<T>(field_name, expected_value);

    to_be_filled->short_name = node.get<std::string>("short_name");
    to_be_filled->time_based = node.get<bool>("time_based");
    to_be_filled->time_failed_threshold = node.get<float>("time_failed_threshold");
    to_be_filled->time_passed_threshold = node.get<float>("time_passed_threshold");
    to_be_filled->base = node.get<std::string>("base");
    to_be_filled->time_fdc_threshold = node.get<float>("time_fdc_threshold");
    to_be_filled->debounce_behavior = node.get<std::string>("debounce_behavior");

}

template <typename T = std::string>
void top_debounce_algorithm(std::string field_name, T expected_value, Custom_t* to_be_filled)
{
    if (to_be_filled == nullptr)
    {
        throw std::runtime_error("[to_be_filled] is nullptr in [top_debounce_algorithm]");
    }

    Node & node = top_debounce_algorithm<T>(field_name, expected_value);

    to_be_filled->short_name = node.get<std::string>("short_name");
    to_be_filled->monitor_internal = node.get<bool>("monitor_internal");
    to_be_filled->base = node.get<std::string>("base");

}


template <typename T = std::string>
Node & top_dtc_all(std::string field_name, T expected_value)
{
    Node & evs = get_root_node().get_child("dtc_all");
    return match_item<T>(evs, field_name, expected_value);
}

template <typename T = std::string>
Node & top_events(std::string field_name, T expected_value)
{
    Node & evs = get_root_node().get_child("events");
    return match_item<T>(evs, field_name, expected_value);
}

template <typename T = std::string>
Node & top_freeze_frames(std::string field_name, T expected_value)
{
    Node & ffs = get_root_node().get_child("freeze_frames");
    return match_item<T>(ffs, field_name, expected_value);
}

template <typename T = std::string>
Node & top_diagnostic_session(std::string field_name, T expected_value)
{
    Node & node = get_root_node().get_child("diagnostic_session");
    return match_item<T>(node, field_name, expected_value);
}

template <typename T = std::string>
Node & top_routines_all(std::string field_name, T expected_value)
{
    Node & node = get_root_node().get_child("routines_all");
    return match_item<T>(node, field_name, expected_value);
}

template <typename T = std::string>
Node & top_diagnostic_session_security_level(std::string field_name, T expected_value)
{
    Node & node = get_root_node().get_child("diagnostic_session_security_level");
    return match_item<T>(node, field_name, expected_value);
}

template <typename T = std::string>
Node & top_routine_parameters_all(std::string field_name, T expected_value)
{
    Node & node = get_root_node().get_child("routine_parameters_all");
    return match_item<T>(node, field_name, expected_value);
}

template <typename T = std::string>
Node & top_datas(std::string field_name, T expected_value)
{
    Node & node = get_root_node().get_child("datas");
    return match_item<T>(node, field_name, expected_value);
}

template <typename T = std::string>
Node & top_IO_all(std::string field_name, T expected_value)
{
    Node & node = get_root_node().get_child("IO_all");
    return match_item<T>(node, field_name, expected_value);
}

template <typename T = std::string>
Node & top_reset_all(std::string field_name, T expected_value)
{
    Node & node = get_root_node().get_child("reset_all");
    return match_item<T>(node, field_name, expected_value);
}

template <typename T = std::string>
Node & top_did_all(std::string field_name, T expected_value)
{
    Node & node = get_root_node().get_child("did_all");
    return match_item<T>(node, field_name, expected_value);
}

template <typename T = std::string>
Node & top_operation_cycle(std::string field_name, T expected_value)
{
    Node & node = get_root_node().get_child("operation_cycle");
    return match_item<T>(node, field_name, expected_value);
}

template <typename T = std::string>
Node & top_extended_data_records(std::string field_name, T expected_value)
{
    Node & node = get_root_node().get_child("extended_data_records");
    return match_item<T>(node, field_name, expected_value);
}

}
}
}

/* #undef EXPORT_SYM */

#define tafDiagGen_tool_version "tafDiagGen_tool_version: 2.0.0_default"
#define tafDiagGen_tool_timestamp  "tafDiagGen_tool_timestamp: 2025_02_18__15_49_32"
#define tafDiagGen_tool_json_md5 "tafDiagGen_tool_json_md5: 35ec45c3a4c0128571008ccb616ae0ca"
#define tafDiagGen_tool_evid_h_md5 "tafDiagGen_tool_evid_h_md5: 82dd9f48525eab54d8ec0768dfd8b9b6"

#define TAFDIAGGEN_JSON_MD5 "35ec45c3a4c0128571008ccb616ae0ca"
#define TAFDIAGGEN_EVID_MD5 "82dd9f48525eab54d8ec0768dfd8b9b6"

#endif /* __CONFIGURATION_HPP__ */
