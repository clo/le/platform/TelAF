/*
 *  Copyright (c) 2021 The Linux Foundation. All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are
 *  met:
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *    * Redistributions in binary form must reproduce the above
 *      copyright notice, this list of conditions and the following
 *      disclaimer in the documentation and/or other materials provided
 *      with the distribution.
 *    * Neither the name of The Linux Foundation nor the names of its
 *      contributors may be used to endorse or promote products derived
 *      from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 *  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 *  ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 *  BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 *  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 *  BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 *  WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 *  OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 *  IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 *  Changes from Qualcomm Innovation Center are provided under the following license:
 *
 *  Copyright (c) 2022-2023 Qualcomm Innovation Center, Inc. All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted (subject to the limitations in the
 *  disclaimer below) provided that the following conditions are met:
 *
 *      * Redistributions of source code must retain the above copyright
 *        notice, this list of conditions and the following disclaimer.
 *
 *      * Redistributions in binary form must reproduce the above
 *        copyright notice, this list of conditions and the following
 *        disclaimer in the documentation and/or other materials provided
 *        with the distribution.
 *
 *      * Neither the name of Qualcomm Innovation Center, Inc. nor the names of its
 *        contributors may be used to endorse or promote products derived
 *        from this software without specific prior written permission.
 *
 *  NO EXPRESS OR IMPLIED LICENSES TO ANY PARTY'S PATENT RIGHTS ARE
 *  GRANTED BY THIS LICENSE. THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT
 *  HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED
 *  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 *  IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
 *  ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 *  DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 *  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 *  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 *  IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 *  OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 *  IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

//-------------------------------------------------------------------------------------------------
/**
 * @page c_tafsimRsim Remote SIM
 *
 * @rst :ref:`API reference <File taf_simRsim_interface.h>` @endrst
 *
 * <HR>
 *
 * The Remote SIM (RSIM) service allows apps to manage a remote SIM instead of the internal SIM card.
 * This service allows a user application to convey APDU requests to the remote SIM and APDU
 * responses to the modem through the RSIM service. The link between the application and the
 * RSIM service is based on the SIM Access Profile (SAP) specification.
 * A remote SIM service is needed for applications that want to communicate related to SIM I/O,
 * e.g., APDU, ATR sends a message to the SIM located at the remote side.
 *
 * @section taf_simRsim_binding IPC interfaces binding
 *
 * The functions of this API are provided by the @b tafRemoteSimSvc application service.
 *
 * The following example illustrates how to bind to the remote SIM service.
 * @verbatim
   bindings:
   {
       clientExe.clientComponent.taf_simRsim -> tafRemoteSimSvc.taf_simRsim
   }
   @endverbatim
 *
 * @section c_taf_simRsim_SendMessage Communication
 *
 * The communication between the application and the remote SIM service uses the SIM Access Profile
 * (SAP) protocol.
 *
 * The latest <a href="https://www.bluetooth.org/DocMan/handlers/DownloadDoc.ashx?doc_id=158740">
 * V11r00 SAP specification</a> is supported by the remote SIM service. All client-mandatory
 * features and some optional features are supported. The table below summarizes all SAP messages
 * supported by the remote SIM service.
 * <table>
 * <tr><th>Feature                                  <th>Associated SAP messages          <th>Support in SAP client <th>RSIM support
 * <tr><td rowspan="5">Connection management        <td>MSGID_CONNECT_REQ                      <td rowspan="5">Mandatory <td>Supported
 * <tr>                                             <td>MSGID_CONNECT_RESP                                               <td>Supported
 * <tr>                                             <td>MSGID_DISCONNECT_REQ                                             <td>Supported
 * <tr>                                             <td>MSGID_DISCONNECT_RESP                                            <td>Supported
 * <tr>                                             <td>MSGID_DISCONNECT_IND                                             <td>Supported
 * <tr><td rowspan="2">Transfer APDU                <td>MSGID_TRANSFER_APDU_REQ                <td rowspan="2">Mandatory <td>Supported
 * <tr>                                             <td>MSGID_TRANSFER_APDU_RESP                                         <td>Supported
 * <tr><td rowspan="2">Transfer ATR                 <td>MSGID_TRANSFER_ATR_REQ                 <td rowspan="2">Mandatory <td>Supported
 * <tr>                                             <td>MSGID_TRANSFER_ATR_RESP                                          <td>Supported
 * <tr><td rowspan="2">Power SIM off                <td>MSGID_POWER_SIM_OFF_REQ                <td rowspan="2">Optional  <td>Supported
 * <tr>                                             <td>MSGID_POWER_SIM_OFF_RESP                                         <td>Supported
 * <tr><td rowspan="2">Power SIM on                 <td>MSGID_POWER_SIM_ON_REQ                 <td rowspan="2">Mandatory <td>Supported
 * <tr>                                             <td>MSGID_POWER_SIM_ON_RESP                                          <td>Supported
 * <tr><td rowspan="2">Reset SIM                    <td>MSGID_RESET_SIM_REQ                    <td rowspan="2">Optional  <td>Supported
 * <tr>                                             <td>MSGID_RESET_SIM_RESP                                             <td>Supported
 * <tr><td>Report Status                            <td>MSGID_STATUS_IND                       <td>Mandatory             <td>Supported
 * <tr><td rowspan="2">Transfer Card Reader Status  <td>MSGID_TRANSFER_CARD_READER_STATUS_REQ  <td rowspan="2">Optional  <td>Supported
 * <tr>                                             <td>MSGID_TRANSFER_CARD_READER_STATUS_RESP                           <td>Supported
 * <tr><td>Error handling                           <td>MSGID_ERROR_RESP                       <td>Mandatory             <td>Supported
 * <tr><td rowspan="2">Set Transport Protocol       <td>MSGID_SET_TRANSPORT_PROTOCOL_REQ       <td rowspan="2">Optional  <td>Not supported
 * <tr>                                             <td>MSGID_SET_TRANSPORT_PROTOCOL_RESP                                <td>Not supported
 * </table>
 *
 * A message can be sent to the service which the sends commands to the modem using @ref taf_simRsim_SendMessage()
 * with the messagePtr, messageNumElements, callbackPtr and contextPtr passed as parameters.
 * This message sending is an asynchronous process. A callback pointer can therefore be passed to
 * @ref taf_simRsim_SendMessage() in order to receive the sending result for the message.
 * @verbatim
   static le_mem_PoolRef_t RsimMsgsPool;
   static uint8_t APDUResp[60] = { 0x06, 0x02, 0x00, 0x00, 0x02, 0x00, 0x00, 0x01, 0x00, 0x00, 0x00, 0x00,
                              0x04, 0x00, 0x00, 0x29, 0x62, 0x25, 0x82, 0x02, 0x78, 0x21, 0x83, 0x02,
                              0x3F, 0x00, 0xA5, 0x0B, 0x80, 0x01, 0x71, 0x83, 0x03, 0x07, 0x93, 0x81,
                              0x87, 0x01, 0x01, 0x8A, 0x01, 0x05, 0x8B, 0x03, 0x2F, 0x06, 0x02, 0xC6,
                              0x06, 0x90, 0x01, 0x00, 0x83, 0x01, 0x01, 0x90, 0x00, 0x00, 0x00, 0x00 };
   static uint8_t APDURespLength = 60;
   RsimMsg_t* rsimPtr1 = le_mem_ForceAlloc(RsimMsgsPool);
   memcpy(rsimPtr1->msg, APDUResp, APDURespLength);
   rsimPtr1->msgLength = APDURespLength;
   rsimPtr1->callback = CallbackHandler;
   rsimPtr1->contextPtr = NULL;
   le_result_t result = taf_simRsim_SendMessage(rsimPtr1->msg, rsimPtr1->msgLength, rsimPtr1->callback, rsimPtr1->contextPtr);
   LE_ASSERT(res == LE_OK);
   @endverbatim
 *
 * Before the tafsimRsim message sending is started, an application registers a callback handler using
 * @ref taf_simRsim_AddMessageHandler().
 * Once the message is sent, the handler will be called indicating the sending status of the message.
 * If sending the message failed, the handler will be called with the error code.
 * @verbatim
   taf_simRsim_MessageHandlerRef_t msgHandlerRef = @ref taf_simRsim_AddMessageHandler(SAPMessageHandler, NULL);
   @endverbatim
 *
 * Applications must use @ref taf_simRsim_RemoveMessageHandler() to release @ref taf_simRsim_MessageHandlerRef_t message
 * handler reference object when it is no longer used.
 *
 * @b WARNING: The remote SIM service supports only one remote SIM card and can therefore be
 *              connected with only one application.
 *
 * @b NOTE:
 *    
 *    - The remote SIM service has to be supported by the modem to be used: check your platform
 *      documentation.
 *    - The remote SIM card should be selected in order to use the remote SIM service.
 *    - As runtime switch is not currently supported, the switch between local and remote SIM card
 *      requires a platform reset to take effect.
 *
 * <HR>
 *
 */
//-------------------------------------------------------------------------------------------------


/**
 * Maximum message size.
 */
DEFINE MAX_MSG_SIZE = (276);

/**
* Minimum message size.
*/
DEFINE MIN_MSG_SIZE = (200);

//-------------------------------------------------------------------------------------------------
/**
 *  Handler to get notifications for specific messages when there are updates
 *  from the modem.
 */
//-------------------------------------------------------------------------------------------------
HANDLER MessageHandler
(
    uint8 message[MAX_MSG_SIZE]    ///< The message.
);

//-------------------------------------------------------------------------------------------------
/**
 * Callback to get send message result.
 */
//-------------------------------------------------------------------------------------------------
HANDLER CallbackHandler
(
    uint8 messageId     IN,    ///< Message ID.
    le_result_t result  IN     ///< Result.
);

//-------------------------------------------------------------------------------------------------
/**
 * Event to notify and send a message.
 */
//-------------------------------------------------------------------------------------------------
EVENT Message
(
    MessageHandler handler    ///< The message handler.
);

//-------------------------------------------------------------------------------------------------
/**
 * Sends a message to the remote SIM service service which sends the message to the modem.
 *
 * @return
 * - LE_OK -- Successfully sent the message.
 * - LE_BAD_PARAMETER -- Length of the message is too long.
 *
 */
//-------------------------------------------------------------------------------------------------
FUNCTION le_result_t SendMessage
(
    uint8 message[MAX_MSG_SIZE]     IN,    ///< The message.
    CallbackHandler callback        IN     ///< The callback handler.
);
