/*
 * Copyright (c) 2024 Qualcomm Innovation Center, Inc. All rights reserved.
 * SPDX-License-Identifier: BSD-3-Clause-Clear
 */

//-------------------------------------------------------------------------------------------------
/**
 * @page c_tafDiag Diag Service
 *
 * @rst :ref:`API reference <File taf_diag_interface.h>` @endrst
 *
 * <HR>
 *
 * Diag Service provides APIs to applications to set enable condition and get enable condition
 * status of enable ID and it also provides notifications for tester tool OFF/ON state changes and
 * API to cancel the file transfer operation.
 *
 * @section c_taf_diag_binding IPC interfaces binding
 *
 * The functions of this API are provided by the @b tafDiagSvc platform service.
 *
 * The following example illustrates how to bind to the diag service.
 *
 * @verbatim
   bindings:
   {
        clientExe.clientComponent.taf_diag -> tafDiagSvc.taf_diag
   }
   @endverbatim
 *
 * @section c_taf_diag_enableCondition_API Enable condition API
 *
 * taf_diag_SetEnableCondition() can be used to set enable condition status for given enable ID
 * to fulfilled or not fulfilled.
 *
 * The following example illustrates how to set the enable condition to fulfilled.
 *
 * @code
   le_result_t result = taf_diagEvent_SetEnableCondition(ENABLE_CONDITION_ID, true);
   if (result != LE_OK)
   {
      LE_ERROR("Fail to set enable condition.");
      return result;
   }
   @endcode
 *
 * taf_diag_GetEnableConditionStatus() can be used to get enable condition status for given
 * enable ID.
 *
 * The following example illustrates how to get the enable condition status.
 *
 * @code
   bool enableStatus = taf_diag_GetEnableConditionStatus(ENABLE_CONDITION_ID);
   if (enableStatus)
   {
      LE_INFO(" Enable status for ENABLE_CONDITION_ID is true");
      return enableStatus;
   }
   else
   {
      LE_INFO(" Enable status for ENABLE_CONDITION_ID is false");
      return enableStatus;
   }
   @endcode
 *
 *@section c_taf_diag_get_service_API Get diag service reference API
 *
 * Get a diag service reference using taf_diag_GetService(). Use the returned
 * reference for subsequent operations.
 *
 * The following example illustrates how to set up a diag server-service-instance.
 *
 * @code
   taf_diag_ServiceRef_t svcRef;    // Service reference

   // Get the service reference.
   svcRef = taf_diag_GetService();
   LE_ERROR_IF((svcRef == NULL), "taf_diag_GetService returns NULL!");
   @endcode
 *
 * @section c_taf_diag_Set_VlanId Set VLAN ID
 *
 * After getting a service reference, an application can set the VLAN ID using taf_diag_SetVlanId().
 * It shall be called before registering the handler for the Tester state notification message and
 * API to cancel file transfer operation.
 *
 * - taf_diag_SetVlanId() -- Sets the VLAN ID.
 *
 * @section c_taf_diag_Select_TargetVlanId Select target VLAN ID
 *
 * An application can select the VLAN ID by calling taf_diag_SelectTargetVlanID(). If the service is
 * bound to multiple VLANs, the application needs to select a VLAN ID for subsequent operation on
 * target VLAN, otherwise the last VLAN ID set using taf_diag_SetVlanId() is considered.
 *
 * @code
   // Select the VLAN ID.
   uint16_t vlanId = 10;
   le_result_t result = taf_diag_SelectTargetVlanID(SvcRef, vlanId);
   if (result != LE_OK)
   {
      LE_ERROR("Fail to select vlan Id.");
      return result;
   }
   @endcode
 *
 * @section c_taf_diag_tester_OFF_ON_state_API Tester OFF/ON notification API
 *
 * After getting the service reference, an application can call taf_diag_AddTesterStateHandler()
 * to register a handler for tester state (OFF/ON) change notification. Once tester state is
 * changed, the handler will be called with the message reference, Vlan ID and current tester State
 * passed as input parameters. Tester state will be OFF(0), whenever there is no UDS communication
 * with IVC for 5 second irrespective of any session. Finally call taf_diag_ReleaseTesterStateMsg()
 * to release the tester state message.
 *
 * @code
   // Rx tester state handler function.
   void TesterStateEventHandler
   (
        taf_diag_TesterStateRef_t rxStateRef,
        uint16_t vlanId,
        taf_diag_State_t currentTesterState,
        void* contextPtr
   )
   {
        // Process after tester state change
   }

   // Register the tester state handler.
   taf_diag_TesterStateHandlerRef_t handlerRef =
       taf_diag_AddTesterStateHandler(svcRef,
            (taf_diag_StateChangeHandlerFunc_t)TesterStateEventHandler, NULL);
   LE_ERROR_IF((handlerRef == NULL), "taf_diag_AddTesterStateHandler returns NULL!");

   // To remove the handler function, when it is not needed.
   taf_diag_RemoveTesterStateHandler(handlerRef);

   // To release the tester state msg after getting the state change notification.
   le_result_t result = taf_diag_ReleaseTesterStateMsg(rxStateRef);
   if (result != LE_OK)
   {
      LE_ERROR("Fail to release msg.");
      return result;
   }
  @endcode
 *
 * @section c_taf_diag_cancel_fileXfer_operation_API Cancel fileXfer API
 *
 * An application can cancel the file transfer operation by calling taf_diag_CancelFileXferAsync().
 * If the service is bound to multiple VLANs, then the application needs to select a VLAN ID first
 * to cancel the file transfer operation of the selected VLAN. If the VLAN ID is not selected then
 * it will cancel the file transfer operation of the last VLAN ID set by taf_diag_SetVlanId().
 *
 * @code
  void CancelFileXferHandlerFunc
  (
      taf_diag_ServiceRef_t svcRef,
      uint16_t vlanId,
      le_result_t result,
      void* contextPtr
  )
  {
      LE_INFO(" CancelFileXferHandlerFunc with vlanId:%d, result: %d", vlanId, result);
  }

   // Call asynchronous API taf_diag_CancelFileXferAsync.
   taf_diag_CancelFileXferAsync(svcRef, CancelFileXferHandlerFunc, NULL);

  @endcode
 *
 * An application can remove the diag service reference by calling taf_diag_RemoveSvc().
 *
 * @code
   le_result_t result = taf_diag_RemoveSvc(svcRef);
   if (result != LE_OK)
   {
      LE_ERROR("Fail to remove service reference.");
      return result;
   }
   @endcode
 *
 * <HR>
 *
 */
//-------------------------------------------------------------------------------------------------

//-------------------------------------------------------------------------------------------------
/**
 * Sets an enable condition.
 *
 * @return
 *     - LE_OK -- Succeeded.
 *     - LE_FAULT -- Failed.
 *
 */
//-------------------------------------------------------------------------------------------------
FUNCTION le_result_t SetEnableCondition
(
    uint8 enableConditionID  IN,     ///< Enable condition ID.
    bool conditionFulfilled  IN      ///< The enable condition is fulfilled (TRUE) or not.
);

//-------------------------------------------------------------------------------------------------
/**
 * Gets an enable condition status.
 *
 * @return
 *     - TRUE -- Succeeded.
 *     - FALSE -- Failed.
 *
 */
//-------------------------------------------------------------------------------------------------
FUNCTION bool GetEnableConditionStatus
(
    uint8 enableConditionID  IN     ///< Enable condition ID.
);

//-------------------------------------------------------------------------------------------------
/**
 * Tester present state.
 */
//-------------------------------------------------------------------------------------------------
ENUM State
{
    OFF = 0x00,    ///< Tester present state is OFF.
    ON             ///< Tester present state is ON.
};

//-------------------------------------------------------------------------------------------------
/**
 * Reference to the Diag service.
 */
//-------------------------------------------------------------------------------------------------
REFERENCE Service;

//-------------------------------------------------------------------------------------------------
/**
 * Reference to the tester present OFF/ON state.
 */
//-------------------------------------------------------------------------------------------------
REFERENCE TesterState;

//-------------------------------------------------------------------------------------------------
/**
 * Gets the reference to a Diag service, if there is no Diag service, a new one will be created.
 *
 * @return
 *     - Reference to the service instance.
 *     - NULL if not allowed to create the service.
 *
 * @b NOTE: The process exits if an invalid reference is passed.
 */
//-------------------------------------------------------------------------------------------------
FUNCTION Service GetService
(
);

//--------------------------------------------------------------------------------------------------
/**
 * Sets VLAN ID to the service. If the VLAN ID is not found or does not exist, it will return an
 * error.
 *
 * @return
 *     - LE_OK -- Succeeded.
 *     - LE_NOT_FOUND -- Reference not found.
 *     - LE_UNSUPPORTED -- VLAN ID is unsupported.
 *
 */
//--------------------------------------------------------------------------------------------------
FUNCTION le_result_t SetVlanId
(
    Service svcRef IN,      ///< Service reference.
    uint16 vlanId IN        ///< VLAN ID.
);

//-------------------------------------------------------------------------------------------------
/**
 * Handler for a tester present state notification.
 */
//-------------------------------------------------------------------------------------------------
HANDLER StateChangeHandler
(
    TesterState stateRef  IN,    ///< Tester state reference.
    uint16 vlanId IN,            ///< VLAN ID.
    State state  IN              ///< Tester state.
);

//-------------------------------------------------------------------------------------------------
/**
 * This event provides information on tester present state change.
 */
//-------------------------------------------------------------------------------------------------
EVENT TesterState
(
    Service svcRef,               ///< Service reference.
    StateChangeHandler handler    ///< Tester present state change handler.
);

//--------------------------------------------------------------------------------------------------
/**
* Releases a Tester state notification message.
*
* @return
*     - LE_OK -- Succeeded.
*     - LE_BAD_PARAMETER -- Invalid stateRef or invalid service of the stateRef.
*/
//--------------------------------------------------------------------------------------------------
FUNCTION le_result_t ReleaseTesterStateMsg
(
    TesterState stateRef  IN    ///< Tester state reference.
);

//-------------------------------------------------------------------------------------------------
/**
 * Selects a target VLAN ID
 *
 * @return
 *     - LE_OK -- Succeeded.
 *     - LE_BAD_PARAMETER -- Invalid svcRef.
 *     - LE_NOT_FOUND -- VLAN ID not found.
 *
 */
//-------------------------------------------------------------------------------------------------
FUNCTION le_result_t SelectTargetVlanID
(
    Service svcRef IN,    ///< Service reference.
    uint16 vlanId IN      ///< VLAN ID.
);

//--------------------------------------------------------------------------------------------------
/**
 * The handler for asynchronous file transfer cancellation result response.
 *
 * @result
 *     - LE_OK -- Succeeded.
 *     - LE_IN_PROGRESS -- The same operation is in progress.
 *     - LE_NOT_POSSIBLE -- No need to perform this operation.
 *
 */
//--------------------------------------------------------------------------------------------------
HANDLER CancelFileXferCallback
(
    Service svcRef                          IN, ///< The profile reference.
    uint16 vlanId                           IN, ///< VLAN ID. If no valid VLAN ID, input 0.
    le_result_t result                      IN  ///< The result of canceling file transfer.
);

//--------------------------------------------------------------------------------------------------
/**
 * Cancels (asynchronously) ongoing file transfer operation.
 *
 */
//--------------------------------------------------------------------------------------------------
FUNCTION CancelFileXferAsync
(
    Service svcRef                          IN, ///< Service reference.
    CancelFileXferCallback handler          IN  ///< The handler.
);

//-------------------------------------------------------------------------------------------------
/**
 * Removes the diag service.
 *
 * @return
 *     - LE_OK -- Succeeded.
 *     - LE_BAD_PARAMETER -- Invalid svcRef.
 */
//-------------------------------------------------------------------------------------------------
FUNCTION le_result_t RemoveSvc
(
    Service svcRef  IN    ///< Service reference.
);