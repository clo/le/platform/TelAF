#!/bin/bash
# Copyright (c) 2022 Qualcomm Innovation Center, Inc. All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted (subject to the limitations in the
# disclaimer below) provided that the following conditions are met:
#
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#
#     * Redistributions in binary form must reproduce the above
#       copyright notice, this list of conditions and the following
#       disclaimer in the documentation and/or other materials provided
#       with the distribution.
#
#     * Neither the name of Qualcomm Innovation Center, Inc. nor the names of its
#       contributors may be used to endorse or promote products derived
#       from this software without specific prior written permission.
#
# NO EXPRESS OR IMPLIED LICENSES TO ANY PARTY'S PATENT RIGHTS ARE
# GRANTED BY THIS LICENSE. THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT
# HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED
# WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
# IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
# ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
# GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
# IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
# OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
# IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

# This file is to onfigure telaf environment variables

CURRENT_DIR_START_CHAR="${0:0:1}"
if [ "$CURRENT_DIR_START_CHAR" == "/" ]
then
    ABS_PATH_TO_CURRENT_DIR="$0"
else
    ABS_PATH_TO_CURRENT_DIR="$(pwd)/$0"
fi
CURDIR=$(realpath $(dirname $(dirname "$ABS_PATH_TO_CURRENT_DIR")))

if [ -d "$TELAF_ROOT" ] && [ "$TELAF_ROOT" != "$CURDIR" ]; then
    echo "Error: The TELAF_ROOT was detected as already being present in this shell environment and inconsistent with the environment to be set. Please use a clean shell when sourcing this environment script."
    exit;
fi

#Ensure presence of TELAF_GLOBAL_TARGET_TOOLCHAIN_PATH environment variable
if [ ! -e "${TELAF_GLOBAL_TARGET_TOOLCHAIN_PATH}" ]; then
    echo "source set_af_env.sh from a clean shell and then run bin/legs"
    exit;
fi

# Configure TelAF root and interface path
export TELAF_ROOT=$(realpath $(dirname $(dirname "$ABS_PATH_TO_CURRENT_DIR")))
export TELAF_INTERFACES=$TELAF_ROOT/interfaces
export TELAF_MNGD_INTERFACES=$TELAF_ROOT/apps/tafMngdServices/interfaces
export LEGATO_ROOT=$TELAF_ROOT/../legato/legato-af

# If there is exactly one argument, legs will execute the argument in the TelAF shell.
if [ $# -eq 1 ]; then
    # User has provided command(s) to be executed in the TelAF build shell.

    # Setup the TelAF framework shell
    cd $LEGATO_ROOT
    source $LEGATO_ROOT/framework/tools/scripts/configlegatoenv
    cd $TELAF_ROOT
    set -e
    # Store user argument in a variable.
    cmds=$1
    # Split the argument passed by the user into separate commands using && as the delimiter
    while [ "$cmds" != "$cmd" ] ;do
        # extract each command
        cmd=${cmds%%&&*}
        # delete the extracted command and the delimiter from the argument string
        cmds="${cmds#$cmd&&}"
        # Execute the command
        echo $cmd
        $cmd
        # Check the return and exit command failed.
        retVal=$?
        if [ $retVal -ne 0 ]; then
            exit $retVal
        fi
    done
    # Exit this shell
    exit
fi

# Setup framework configuration in a sub-shell and start interperting stdin interactively.
bash -i  <<EOF
cd $LEGATO_ROOT
source $LEGATO_ROOT/framework/tools/scripts/configlegatoenv
if [ -v ORIGIN_DIR ]; then cd $ORIGIN_DIR; else cd $TELAF_ROOT; fi
exec <> /dev/tty
EOF
